import { CamachoAppPage } from './app.po';

describe('camacho-app App', () => {
  let page: CamachoAppPage;

  beforeEach(() => {
    page = new CamachoAppPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
