import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Router } from '@angular/router';
import { Dataset } from '../classes/dataset'

import { Observable } from 'rxjs/Rx';

@Injectable()
export class DatasetsService {

  url: string = 'http://localhost:8080/web-api/v1/datasets/';

  constructor(
    private http: Http,
  ) { }

  list() : Observable<Dataset[]> {
    let token = localStorage.getItem('X-Auth-Token');
    let bodyString = null;//'{}';//JSON.stringify(body); // Stringify payload

    let headers = new Headers({
      'Content-Type': 'application/json',
      'X-Auth-Token': token

    }); // ... Set content type to JSON
    let options = new RequestOptions({ headers: headers }); // Create a request option

    return this.http.get(this.url, options).map((res: Response) => res.json())
      //...errors if any
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));

  }

  findDataset(id: string) : Observable<Dataset> {
    let token = localStorage.getItem('X-Auth-Token');
    let bodyString = null;//'{}';//JSON.stringify(body); // Stringify payload

    let headers = new Headers({
      'Content-Type': 'application/json',
      'X-Auth-Token': token

    }); // ... Set content type to JSON
    let options = new RequestOptions({ headers: headers }); // Create a request option

    return this.http.get(this.url + id, options).map((res: Response) => res.json())
      //...errors if any
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));

  }

  findParameters(id: string) : Observable<Dataset> {
    let token = localStorage.getItem('X-Auth-Token');
    let bodyString = null;//'{}';//JSON.stringify(body); // Stringify payload

    let headers = new Headers({
      'Content-Type': 'application/json',
      'X-Auth-Token': token

    }); // ... Set content type to JSON
    let options = new RequestOptions({ headers: headers }); // Create a request option

    return this.http.get(this.url + id + '/' + 'parameters', options).map((res: Response) => res.json())
      //...errors if any
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));

  }

  delete(id: string) : Observable<Response> {
    let token = localStorage.getItem('X-Auth-Token');
    let bodyString = null;//

    let headers = new Headers({
      'Content-Type': 'application/json',
      'X-Auth-Token': token

    }); // ... Set content type to JSON
    let options = new RequestOptions({ headers: headers }); // Create a request option

    return this.http.delete(this.url + id , options).map((res: Response) => res.json())
      //...errors if any
      .catch((error: any) => this.throwError(error));

  }

  private throwError(error: Response) {

    return Observable.throw(error)
  }

}
