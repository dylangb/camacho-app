import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Router } from '@angular/router';

import { Observable } from 'rxjs/Rx';

// Import RxJs required methods
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class AuthService {

  constructor
    (
    private http: Http,
    
    ) { }

  url: string = 'http://localhost:8080/web-api/v1/authenticate/';

  login(username: string, password: string) {
    let bodyString = null;//'{}';//JSON.stringify(body); // Stringify payload
    let headers = new Headers({
      'Content-Type': 'application/json',
      'user_name': username,
      'password': password
    }); // ... Set content type to JSON
    let options = new RequestOptions({ headers: headers }); // Create a request option

    return this.http.post(this.url, bodyString, options).map((res: Response) => res)
      //...errors if any
      .catch((error: any) => this.throwError(error));

  }

   private throwError(error: any) {

    return Observable.throw(error)
  }

}
