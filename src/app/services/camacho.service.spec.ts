import { TestBed, inject } from '@angular/core/testing';

import { CamachoService } from './camacho.service';

describe('CamachoServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CamachoService]
    });
  });

  it('should ...', inject([CamachoService], (service: CamachoService) => {
    expect(service).toBeTruthy();
  }));
});
