import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { Router } from '@angular/router';
import { Parameter } from '../classes/parameter';
import { Dataset } from '../classes/dataset';
import { Chart } from '../classes/chart';

import { Observable } from 'rxjs/Rx';

@Injectable()
export class ChartService {

  url: string = 'http://localhost:8080/web-api/v1/chart/';

  constructor(
    private http: Http,
  ) { }


  chart(id: string, parameters: Parameter[]): Observable<Chart> {
    let token = localStorage.getItem('X-Auth-Token');
    let bodyString = null;//'{}';//JSON.stringify(body); // Stringify payload

    let headers = new Headers({
      'Content-Type': 'application/json',
      'X-Auth-Token': token

    }); // ... Set content type to JSON
    let options = new RequestOptions({ headers: headers }); // Create a request option

    let params: URLSearchParams = new URLSearchParams();
    let paramsList = '';
    for (let parameter of parameters) {
      if (paramsList.length > 0) {
        paramsList += ',';
      }
      paramsList += parameter.name;
    }
    params.set('parameters', paramsList);
    //todo: add more options here
    options.search = params;

    return this.http.get(this.url + id, options).map((res: Response) => res.json())
      //...errors if any
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));

  }

}
