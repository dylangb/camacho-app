import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';

import { DataTableModule, SharedModule, ContextMenuModule, MessagesModule, GrowlModule, RadioButtonModule } from 'primeng/primeng';
import { DragulaModule} from 'ng2-dragula';
import { NgDygraphsModule } from 'ng-dygraphs';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { DatasetsComponent } from './components/datasets/datasets.component';
import { ChartComponent } from './components/chart/chart.component';

import { AuthService} from './services/auth.service';
import { CamachoService} from './services/camacho.service';
import { DatasetsService} from './services/datasets.service';
import { ChartService} from './services/chart.service';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DatasetsComponent,
    ChartComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    DataTableModule,
    ContextMenuModule,
    DragulaModule,
    NgDygraphsModule,
    MessagesModule,
    GrowlModule,
    RadioButtonModule,
    NgbModule.forRoot(),
    RouterModule.forRoot([
      {
        path: '',
        component: LoginComponent
      },
      {
        path: 'login',
        component: LoginComponent
      },
      {
        path: 'datasets',
        component: DatasetsComponent
      }
      ,
      {
        path: 'chart/:id',
        component: ChartComponent
      }
    ]),

  ],
  providers: [
    AuthService,
    CamachoService,
    DatasetsService,
    ChartService,
    ],

  bootstrap: [AppComponent]
})
export class AppModule { }
