import { Component, OnInit, ViewChild } from '@angular/core';
import { Response } from '@angular/http';
import { DatasetsService } from '../../services/datasets.service';
import { Dataset } from '../../classes/dataset';
import { Meta } from '../../classes/meta';
import { ContextMenu, MenuItem } from 'primeng/primeng';
import { Router } from '@angular/router';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'app-datasets',
  templateUrl: './datasets.component.html',
  styleUrls: ['./datasets.component.css']
})
export class DatasetsComponent implements OnInit {

  constructor(
    private datasetsService: DatasetsService,
    private router: Router,
    private modalService: NgbModal,
  ) { }

  msgs: any[] = [];
  datasets: Dataset[];
  menuItems: MenuItem[];
  selectedDataset: Dataset;
  @ViewChild('content') content: any;

  ngOnInit() {

    this.menuItems = [
      { label: 'Chart', icon: 'fa-line-chart', command: (event) => this.chartDataset(this.selectedDataset) },
      { label: 'View Meta', icon: 'fa-search', command: (event) => this.viewMeta(this.selectedDataset) },
      { label: 'Delete', icon: 'fa-close', command: (event) => this.deleteDataset(this.selectedDataset) }
    ];

    this.datasetsService.list().subscribe(
      datasets => this.prepareForDisplay(datasets),
      error => this.handleError(error)
    )
  }

  private prepareForDisplay(datasets: Dataset[]) {
    this.datasets = datasets;
    for (let dataset of this.datasets) {
      this.parseMeta(dataset);
    }

  }

  private parseMeta(dataset: Dataset) {
    for (var key in dataset.metaInfo) {
      if (dataset.metaInfo.hasOwnProperty(key)) { // this will check if key is owned by data object and not by any of it's ancestors
        if (!dataset.metadata) {
          dataset.metadata = [];
        }
        dataset.metadata.push(new Meta(key, dataset.metaInfo[key]));
      }
    }

  }

  private handleError(error: Response) {
    if (error.status == 401) {
      this.router.navigate(['/login'])
    }
    this.msgs.push({ severity: 'warn', summary: 'Error', detail: JSON.stringify(error) })

  }

  private handleResponse(response: Response) {

    this.msgs.push({ severity: 'success', summary: 'Success', detail: 'Deleted successfully' });
    this.ngOnInit();
  }

  private chartDataset(dataset: Dataset) {
    //Redirect to chart page
    //In chart page, on click "chart" -> launch modal containing chart
    console.log(dataset.datasetId.toString());
    this.router.navigate(['/chart', dataset.datasetId.toString()]);
  }

  private viewMeta(dataset: Dataset) {
    this.open(this.content);
    this.selectedDataset = Object.assign({}, dataset);

  }

  private deleteDataset(dataset: Dataset) {
    this.datasetsService.delete(dataset.datasetId.toString()).subscribe(
      response => this.handleResponse(response),
      error => this.handleError(error)
    )
  }

  open(content) {

    this.modalService.open(content).result.then((result) => {
      console.log(result);
    }, (reason) => {
      console.log(reason);
    });

  }

}
