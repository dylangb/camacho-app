import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
import { Response, Headers} from '@angular/http';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(
    private authService: AuthService,
    private router: Router
  ) { }

  msgs: any[] = [];
  private username: string;
  private password: string;
  ngOnInit() {
  }

  login() {
    this.authService.login(this.username, this.password)
      .subscribe(
      response => this.handleResponse(response),
      error => this.handleError(error)
      )
  }

  private handleResponse(response: Response) {
    if (response.ok) {
      let token = response.headers.get('x-subject-token');
      localStorage.setItem('X-Auth-Token', token);
      this.router.navigate(['datasets']);
    } else{
      this.msgs.push({severity:'info', summary:'Error', detail:JSON.stringify(response.json)})
    }

  }


  private handleError(error: Response) {
    if (error.status == 401) {
      this.msgs.push({severity:'warn', summary:'Unable to authenticate', detail:error.statusText})
    } else {
      this.msgs.push({severity:'error', summary:'Server error', detail:error.statusText})
    }
    
  }

}
