import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';
import { DatasetsService } from '../../services/datasets.service';
import { ChartService } from '../../services/chart.service';
import { Dataset } from '../../classes/dataset';
import { Parameter } from '../../classes/parameter';
import { Chart } from '../../classes/chart';
import { DragulaService } from 'ng2-dragula/ng2-dragula';
import { NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';

declare var Dygraph: any;

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.css']
})
export class ChartComponent implements OnInit {

  constructor(private route: ActivatedRoute,
    private datasetsService: DatasetsService,
    private dragulaService: DragulaService,
    private chartService: ChartService,
    private modalService: NgbModal,
    private router: Router,) {
    //   dragulaService.setOptions('first-bag', {
    //   moves: function (el, container, handle) {
    //     return handle.className === 'handle';
    //   }
    // });

  }


  msgs: any[] = [];
  growls : any[] = [];
  datasetId: string;
  dataset: Dataset;
  chartObject: Chart;
  parameters: Parameter[] = [];
  chartParameters: Parameter[] = [];
  options: Object = {};
  chartType: string = 'scatter';

  private sub: any;
  @ViewChild('dygraph') dygraph: ElementRef;
  g;

  ngOnInit() {

    this.sub = this.route.params.subscribe(params => {

      this.datasetId = params['id']; // (+) converts string 'id' to a number
      console.log('received: ' + this.datasetId)
      this.datasetsService.findDataset(this.datasetId)
        .subscribe(dataset => this.prepareForDisplay(dataset),
        error => this.handleError(error));//get the dataset

      this.datasetsService.findParameters(this.datasetId)
        .subscribe(parameters => this.parseParameters(parameters),
        error => this.handleError(error));//get the parameters
    });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  private prepareForDisplay(dataset: Dataset) {
    this.dataset = dataset;

  }

  private parseParameters(paramsDict: Object) {
    for (var key in paramsDict) {
      if (paramsDict.hasOwnProperty(key)) { // this will check if key is owned by data object and not by any of it's ancestors
        this.parameters.push(new Parameter(key, paramsDict[key]));
      }
    }

  }

  private handleError(error: Response) {
    if (error.status == 401) {
      this.router.navigate(['/login'])
    }
    this.msgs.push({ severity: 'error', summary: 'Error', detail: JSON.stringify(error) })
  }

  chart() {
    if(this.chartParameters.length == 0) {
      this.growls.push({severity:'warn', summary:'Please drag at least one parameter into the \'Chart Parameters\' area', detail:''});
    } else {
    this.chartService.chart(this.datasetId, this.chartParameters)
      .subscribe(chart => this.buildChart(chart),
      error => this.handleError(error))
    }
  }

  private handleRadioButtonChanged(){
    if (this.chartType === 'scatter') {
      this.options['drawPoints'] = true;
      this.options['strokeWidth'] = 0.0;
    } else if (this.chartType === 'scatter'){
      this.options['drawPoints'] = false;
      this.options['strokeWidth'] = 1.0;
    }
    if(this.g) {
      this.g.updateOptions(this.options);
    }
    

  }

  private buildChart(chart: Chart) {

    this.chartObject = chart;
    this.options['labels'] = this.chartObject.labels;
    this.options['animatedZooms'] = true;
    if (this.chartType === 'scatter') {
      this.options['drawPoints'] = true;
      this.options['strokeWidth'] = 0.0;
    } else if (this.chartType === 'line'){
      this.options['drawPoints'] = false;
      this.options['strokeWidth'] = 1.0;
    }
    
    this.options['title'] = this.dataset.name;
   
    this.g = new Dygraph(
      this.dygraph.nativeElement,
      this.chartObject.data,
      this.options
    )

  }

  



}
