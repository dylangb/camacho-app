import { Meta } from './meta'

export class Dataset {
    name: string;
    datasetId: number;
    project: string;
    date: Date;
    deviceToken: string;
    itemCountFromDB: number;
    metaInfo: Object;
    metadata: Meta[] = [];
    constructor (name: string, datasetId: number, project: string, date: Date, deviceToken: string, itemCountFromDB: number, metaInfo: Object) {
        this.name = name;
        this.datasetId = datasetId;
        this.project = project;
        this.date = date;
        this.deviceToken = deviceToken;
        this.itemCountFromDB = itemCountFromDB;
        this.metaInfo = metaInfo;

    }
}
